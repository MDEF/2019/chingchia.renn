/*
   Uses a high value resistor e.g. 10M between send pin and receive pin
   Resistor effects sensitivity, experiment with values, 50K - 50M. Larger resistor values yield larger sensor values.
   Receive pin is the sensor pin - try different amounts of foil/metal on this pin
*/

#include <CapacitiveSensor.h>
// 10M resistor between pins 4 & 8, pin 8 is sensor pin, add a wire and or foil
CapacitiveSensor   cs_7_8 = CapacitiveSensor(7, 8);
int speaker = 9;

void setup()
{
  cs_7_8.set_CS_AutocaL_Millis(0xFFFFFFFF);
  Serial.begin(9600);
}

void loop()
{
  long start = millis();
  long sensor =  cs_7_8.capacitiveSensor(50);

  Serial.println(sensor);
  if (sensor >= 100) {

    int pitch = map(sensor, 100, 1500, 131, 1047);
    tone(speaker, pitch);

  } else {
    noTone(speaker);
  }

  delay(10);

}
