int flag_led = 0;
byte Buttonpin = 7;

void setup() {
  Serial.begin(9600);
  pinMode(Buttonpin, INPUT);
}

void loop() {
  if (digitalRead(Buttonpin)) {
    flag_led = 1;
  } else {
    flag_led = 0;
  }
  Serial.print(flag_led);
  delay(1000);
}
