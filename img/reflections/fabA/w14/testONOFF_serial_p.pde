import processing.serial.*;

Serial myPort;  // Create object from Serial class
int val;        // Data received from the serial port

import oscP5.*;  
import netP5.*;

OscP5 oscP5;
NetAddress dest;

void setup() 
{
  size(200, 200);
  String portName = Serial.list()[0];
  myPort = new Serial(this, portName, 9600);
  
  oscP5 = new OscP5(this, 12000); 
  dest = new NetAddress("127.0.0.1", 6448);

}

void oscEvent(OscMessage theOscMessage) {
  if (theOscMessage.checkAddrPattern("/output_1")==true) 
  {
    myPort.write('H');
    println("on");
  } else if (theOscMessage.checkAddrPattern("/output_2")==true) 
  {     
    myPort.write('L');
    println("off");
  } else {
    println("Unknown OSC message received");
  }
}

void draw() {
  
}
