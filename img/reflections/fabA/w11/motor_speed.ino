const int motor = 9;
const int pot = A0;

void setup()  
{  
  pinMode(motor, OUTPUT);
  Serial.begin(9600);
}    

void loop()  
{  
  int reading = analogRead(pot);  
  int spd = map(reading, 0, 1023, 0, 255);
  analogWrite(motor, spd);
  Serial.print("Voltage: ");
  Serial.println(spd/51.0);
  delay(50);  
}  
